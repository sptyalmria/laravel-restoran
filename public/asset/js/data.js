const daftarMenu = () => {
    fetch("api/getMenu")
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            let menuResto = "";
            data.map((item) => {
                menuResto += `      
                    <div class="col col-md-3 mb-3 mt-3">
                        <div class="card">
                          <img src="${item.gambar}" class="card-img-top" height="250px">
                              <div class="card-body text-center">
                                <h5 class="card-title">${item.nama}</h5>
                                <p class="card-text">${item.deskripsi}</p>
                                <p class="card-text">Harga: Rp${item.harga}</p>
                                <button class="btn btn-danger">Buy Now</button>
                              </div>
                         </div>
                      </div>`;
            })

            const menuList = document.querySelector("#catalogMenu");
            menuList.innerHTML = menuResto;
        })
    };

daftarMenu();

//Map

let jsondata = "";
let apiUrl = "api/getMenu"

async function getJson(url) {
    let response = await fetch(url);
    let data = await response.json()
    return data;
}

async function allData() {

    jsondata = await getJson(apiUrl)
    var daftarMenu = (item) => {
        const daftarMenu = document.querySelector("#catalogMenu");

        daftarMenu.innerHTML += `
        <div class="col col-md-3 mb-3 mt-3">
            <div class="card">
              <img src="${item.gambar}" class="card-img-top" height="250px">
                  <div class="card-body text-center">
                    <h5 class="card-title">${item.nama}</h5>
                    <p class="card-text">${item.deskripsi}</p>
                    <p class="card-text">Harga: Rp${item.harga}</p>
                    <button class="btn btn-danger">Buy Now</button>
                  </div>
             </div>
        </div>`;
    }
    jsondata.map(daftarMenu);

//Filter
var searchData = document.querySelector("#input-keyword");
    searchData.addEventListener('click', () => {
        const search = jsondata.filter((item) => {
            const input = document.querySelector("#input-keyword");
            const namaItem = item.namaMakanan.toLowerCase();
            const typeInput = input.value.toLowerCase();

            return namaMenu.includes(typeInput);
        })
        document.querySelector("#catalogMenu").innerHTML = ``;
        search.map(daftarMenu);
    });

//Reduce
const jumlahPesanan = []
const buttonPesan = document.querySelectorAll('.btn-danger');
const elmntJumlahPesanan = document.querySelector('.jumlah-pesanan > p');

buttonPesan.forEach((item, index)=>{
item.addEventListener('click', ()=>{
  jumlahPesanan.push(1);
  
  
  const hasil = jumlahPesanan.reduce((accumulator, currentValue)=>{
    return accumulator + currentValue;
    }, 0);


  elmntJumlahPesanan.innerHTML = hasil;
  })
});

allData();